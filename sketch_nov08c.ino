//JOYSTICK
#define JOY_X A0
#define JOY_Y A1
#define JOY_BTN 11

int xVal,yVal;
bool joyMoved = false;

//1 DIGIT
#define PIN_A 8
#define PIN_B 7
#define PIN_C 5
#define PIN_D 4
#define PIN_E 3
#define PIN_F 9
#define PIN_G 10
#define PIN_BTN 6

int segments[7] = {PIN_A, PIN_B, PIN_C, PIN_D, PIN_E, PIN_F, PIN_G};

int digit = 5; // sa fie initial cifra din mijloc, la fel cum si joystickul sta pe mijloc

//MATRICE CIFRE
//segmentul                A  B  C  D  E  F  G
byte matrix_digit[10][7] = {{1, 1, 1, 1, 1, 1, 0}, // 0
                          {0, 1, 1, 0, 0, 0, 0}, // 1
                          {1, 1, 0, 1, 1, 0, 1}, // 2
                          {1, 1, 1, 1, 0, 0, 1}, // 3
                          {0, 1, 1, 0, 0, 1, 1}, // 4
                          {1, 0, 1, 1, 0, 1, 1}, // 5
                          {1, 0, 1, 1, 1, 1, 1}, // 6
                          {1, 1, 1, 0, 0, 0, 0}, // 7
                          {1, 1, 1, 1, 1, 1, 1}, // 8
                          {1, 1, 1, 1, 0, 1, 1}  // 9
                        };

//FUNCTIE AFISARE CIFRA
void display_digit(byte digit) 
{
  for (int count = 0; count < 7; count++) {
  digitalWrite(segments[count], matrix_digit[digit][count]);
  }
}

void setup() {
  pinMode(JOY_X, INPUT);
  pinMode(JOY_Y, INPUT);
  pinMode(JOY_BTN, INPUT_PULLUP);

  pinMode(PIN_A, OUTPUT);
  pinMode(PIN_B, OUTPUT);
  pinMode(PIN_C, OUTPUT);
  pinMode(PIN_D, OUTPUT);
  pinMode(PIN_E, OUTPUT);
  pinMode(PIN_F, OUTPUT);
  pinMode(PIN_G, OUTPUT);

  display_digit(5); // initial afisam cifra din mijloc
}

void loop() 
{
  xVal = analogRead(JOY_X);

  if (xVal < 400 && joyMoved == false) 
  { // daca trag spre stanga axei X, sa scada cifra
    if (digit > 0) 
    {
      digit--;
    }
    else 
      digit = 9;
    joyMoved = true;
  }

  if (xVal > 600 && joyMoved == false) 
  { 
    // daca trag spre dreapta axei X, sa scada cifra
    if (digit < 9) 
    {
      digit++;
    }
    else 
      digit = 0;
    joyMoved = true;
  }

  yVal = analogRead(JOY_Y);

  if (yVal < 400 && joyMoved == false) 
  { // daca trag spre stanga axei Y, sa scada cifra
    digit+=2;
    if (digit >= 10)
      digit-=10;
    joyMoved = true;
  }

  if (yVal > 600 && joyMoved == false) 
  { 
    // daca trag spre dreapta axei Y, sa scada cifra
    digit-=2;
    if (digit < 0)
      digit+=10;
    joyMoved = true;
  }

  if (xVal >= 400 && xVal <= 600 && yVal >= 400 && yVal <= 600)
    joyMoved = false; // daca au dat drumul la joystick, sa nu se intample nimic


  display_digit(digit);
}
