#define PIN_A 12
#define PIN_B 8
#define PIN_C 5
#define PIN_D 3
#define PIN_E 2
#define PIN_F 11
#define PIN_G 6
#define PIN_DP 4
#define D1 7
#define D2 9
#define D3 10
#define D4 13

int segments[7] = {PIN_A, PIN_B, PIN_C, PIN_D, PIN_E, PIN_F, PIN_G};  
int digits[4] = {D1, D2, D3, D4};   // array pentru digiti

void writeNumber(int);              // afiseaza numarul curent  
void showDigit(int);                // activeaza digitul primit ca parametru
int currentNumber = 0;
unsigned long delayCounting = 1000;    // timp de incrementare numar
unsigned long lastIncreasing = 0;   // variabila folosita pt millis

boolean numbers[10][7] = 
{
  {1,1,1,1,1,1,0},
  {0,1,1,0,0,0,0},
  {1,1,0,1,1,0,1},
  {1,1,1,1,0,0,1},
  {0,1,1,0,0,1,1},
  {1,0,1,1,0,1,1},
  {1,0,1,1,1,1,1},
  {1,1,1,0,0,0,0},
  {1,1,1,1,1,1,1},
  {1,1,1,1,0,1,1}
};

void setup() 
{
  for (int i = 0; i < 7; i++)
  {
    pinMode(segments[i], OUTPUT);  
  }
  for (int i = 0; i < 4; i++)
  {
    pinMode(digits[i], OUTPUT);  
  }
  Serial.begin(9600);
}

void loop() 
{
  int number;
  int digit;
  int cifra;
  Serial.println(currentNumber);

  number = currentNumber;
  digit = 0;
  
  while (number != 0) 
  {
    cifra = number % 10;  // ia ultima cifra a numarului
    showDigit(digit);
    writeNumber(cifra);
    delay(5);
    digit++;              // se trece la urmatorul digit
    number = number / 10;
  }
  // incrementare numar
  if (millis() - lastIncreasing >= delayCounting) 
  {
    currentNumber = (currentNumber + 1) % 10000;
    lastIncreasing = millis();
  }
}

void writeNumber(int number) 
{
  for (int i = 0; i < 7; i++) 
  {
    digitalWrite(segments[i], numbers[number][i]);  
  } 
}

void showDigit(int num) 
{
  for (int i = 0; i < 4; i++) 
  {
    digitalWrite(digits[i], HIGH);
  }
  digitalWrite(digits[num], LOW);
}
