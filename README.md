# Intermediar  
Teme de laborator  
Manea Andrei - grupa 233  
Laborator Robotica  
Laborant - Teodor Marina  

##	Laborator 2 - sketch_oct25e  

![sketch_oct25e](https://user-images.githubusercontent.com/37237590/50546824-d7cfd500-0c36-11e9-9b87-0a9c01087f63.jpg)

##	Laborator 4 
*	sketch_nov08c  
	*	Am adaugat miscarea joystickului pe axa Oy, care face numerele sa se deplaseze din 2 in 2  

![sketch_nov08c](https://user-images.githubusercontent.com/37237590/50546828-eddd9580-0c36-11e9-929f-25f64b37b8d3.png)

*	sketch_dec30a  
	*	Counter-ul cu 4 7 segment display, facut sa numere secundele si sa afise cu un delay de 5m.  

![sketch_dec30a](https://user-images.githubusercontent.com/37237590/50546899-7f99d280-0c38-11e9-9a7e-e05256709cdc.png)  

*	sketch_dec30b  
	*	Joysticul modificat care schimba cifrele, tinut cu firele in sus. Pe axa Ox alege cifra, iar pe Oy o modifica.   
![sketch_dec30b](https://user-images.githubusercontent.com/37237590/50550638-e3de8580-0c7c-11e9-8c65-8a6590281505.png)  

*	sketch_dec30c  
	*	Incercare proprie, un led RGB, controlat de 2 potentiometre. Primul controleaza canalul de red, al 2lea de G, iar cel de B este dat in cod.
![sketch_dec30c](https://user-images.githubusercontent.com/37237590/50550640-e9d46680-0c7c-11e9-8caf-bb4104f06e9d.jpg)  

## Video:  
https://drive.google.com/drive/folders/14W60x-XpWkR7_4yZM0CDc4_dxy3AwFWJ?usp=sharing  