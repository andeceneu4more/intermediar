#define redPin 11
#define greenPin 10
#define bluePin 9
#define redIn A0
#define greenIn A1
#define buzzPin 6
// Catod Comun
void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);  
  pinMode(redIn, INPUT);
  pinMode(greenIn, INPUT);
  pinMode(buzzPin, OUTPUT);
  Serial.begin(9600);
}
int R, G, B = 255;
void loop()
{
    int redVal = analogRead(redIn);
    int greenVal = analogRead(greenIn);
    R = map(redVal, 0, 1023, 0, 255);
    G = map(greenVal, 0, 1023, 0, 255);
    Serial.print(R);
    Serial.print(" ");
    Serial.print(G);
    Serial.print(" ");
    Serial.println(B);
    setColor(R, G, B);
    if (R == 255 && G == 255 && B == 255)
    {
        tone(buzzPin, 1000);
        //delay(1000);
    }
}
 
void setColor(int red, int green, int blue)
{
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);  
}
/* #define COMMON_ANODE
   #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
    blue = 255 - blue;
  #endif
*/
