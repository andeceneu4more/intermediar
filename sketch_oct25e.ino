int trigPin=5;
int echoPin=6;
int RED_LED=4, GREEN_LED=3, BLUE_LED=2;
int duration,distance;
void setup() 
{
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(BLUE_LED, OUTPUT);
  Serial.begin(9600);
}

void loop() 
{
  digitalWrite(trigPin,LOW);
  digitalWrite(RED_LED,LOW);
  digitalWrite(GREEN_LED,LOW);
  digitalWrite(BLUE_LED,LOW);
  delayMicroseconds(2);
  //reset
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  //launch signal
  digitalWrite(trigPin,LOW);
  //STOP wave
  duration=pulseIn(echoPin, HIGH);
  distance=duration*0.034/2;
  //measure
  if (distance < 70)
  {
    digitalWrite(RED_LED,HIGH);
  }
  if (distance < 150)
  {
    digitalWrite(GREEN_LED,HIGH);
  }
  if (distance < 350)
  {
    digitalWrite(BLUE_LED,HIGH);
  }
      


}
