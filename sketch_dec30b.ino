// selectie //digiti folosind joystickul(axa x) si modificarea valorilor lor // folosind axa y
#define PIN_A 12
#define PIN_B 8
#define PIN_C 5
#define PIN_D 3
#define PIN_E 2
#define PIN_F 11
#define PIN_G 6
#define PIN_DP 4
#define D1 7
#define D2 9
#define D3 10
#define D4 13

#define Ox A4
#define Oy A3
#define JOY_BUTTON A5

int segments[7] = {PIN_A, PIN_B, PIN_C, PIN_D, PIN_E, PIN_F, PIN_G};
int digits[4] = {D1, D2, D3, D4};   // array pentru digiti

void writeNumber(int);
void showDigit(int);
int currentNumber[4] = {0, 0, 0, 0};
int joyMoved = 0;
int selectedDigit = 0;
bool buttonState = false;

unsigned long delayCounting = 50;
unsigned long lastIncreasing = 0;

int numbers[10][7] = {
  {1,1,1,1,1,1,0},
  {0,1,1,0,0,0,0},
  {1,1,0,1,1,0,1},
  {1,1,1,1,0,0,1},
  {0,1,1,0,0,1,1},
  {1,0,1,1,0,1,1},
  {1,0,1,1,1,1,1},
  {1,1,1,0,0,0,0},
  {1,1,1,1,1,1,1},
  {1,1,1,1,0,1,1}
};

void setup() 
{
  for (int i = 0; i < 7; i++)
  {
    pinMode(segments[i], OUTPUT);  
  }
  for (int i = 0; i < 4; i++)
  {
    pinMode(digits[i], OUTPUT);  
  }
  pinMode(Ox, INPUT);
  pinMode(Oy, INPUT);
  pinMode(JOY_BUTTON, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() 
{

  int x = analogRead(Ox);
  int y = analogRead(Oy);
  Serial.print("X: ");
  Serial.print(x);
  Serial.print(" - Y: ");
  Serial.println(y);
  if (x > 1000 && joyMoved == 0) 
  {
    selectedDigit++;
    if (selectedDigit == 4)
        selectedDigit = 0;
    joyMoved = 1;
  }
  if (x < 5 && joyMoved == 0) 
  {
    selectedDigit--;
    if (selectedDigit < 0)
        selectedDigit = 3;
    joyMoved = 1;
  }
  if (y < 5 && joyMoved == 0) 
  {
    if (currentNumber[selectedDigit] < 9) 
    {
      currentNumber[selectedDigit]++;
    }
    else 
    {
      currentNumber[selectedDigit] = 0;
    }
    joyMoved = 1;
  }
  
  if (y > 1000 && joyMoved == 0) 
  {
    if (currentNumber[selectedDigit] > 0) 
    {
      currentNumber[selectedDigit]--;
    }
    else 
    {
      currentNumber[selectedDigit] = 9;
    }
    joyMoved = 1;
  }
  
  if (y > 400 && y < 600 && x > 400 && x < 600) 
  {
    joyMoved = 0;  
  }

  for (int i = 0; i < 4; i++) 
  {
    showDigit(i);
    writeNumber(currentNumber[i]);
    delay(5);  
  }
}
 
void writeNumber(int number) 
{
  for (int i = 0; i < 7; i++) 
  {
    digitalWrite(segments[i], numbers[number][i]);  
  }
}

void showDigit(int num) 
{

  for (int i = 0; i < 4; i++) 
  {
    digitalWrite(digits[i], HIGH);
  }
  
  digitalWrite(digits[num], LOW);
}
